import React, { Component } from 'react';
import './App.css';
import data from '../data/content.json';

class App extends Component {
  constructor() {
    super()
    this.state = {
      index: 0,
      open: true
    }
    this.toggle = this.toggle.bind(this)
    this.prev = this.prev.bind(this)
    this.next = this.next.bind(this)
  }

  componentDidMount() {
    this.setState({
      index: 0
    })
  }

  toggle(e) {
    this.setState({
      open: !this.state.open
    })
  }

  prev(e) {
    e.preventDefault()
    this.setState({
      index: this.state.index - 1
    })
  }

  next(e) {
    e.preventDefault()
    this.setState({
      index: this.state.index + 1
    })
  }

  render() {
    let thumbnail = data.content[this.state.index].thumbnail
    let description = data.content[this.state.index].description
    let arrowdirection = this.state.open ? 'arrow-up' : 'arrow-down'
    let prev = this.state.index > 0 ? data.content[this.state.index - 1].title : ''
    let next = this.state.index < (data.content.length - 1) ? data.content[this.state.index + 1].title : ''

    return (
      <div className="App">
        <div className={`Accordion ${arrowdirection}`} onClick={this.toggle}>
          <h2>{data.title}</h2>
        </div>
        { this.state.open &&
          <div className="Content">
            { thumbnail &&
              <div className="thumbnail"><img src={`assets/images/${thumbnail}`} alt=""/></div>
            }
            { description &&
              <p className="description" dangerouslySetInnerHTML={{ __html: description }}></p>
            }
            <div className="nav">
              <div className="prev">
                { prev &&
                  <a href="#" onClick={this.prev}>{prev}</a>
                }
              </div>
              <div className="next">
                { next &&
                  <a href="#" onClick={this.next}>{next}</a>
                }
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default App;
